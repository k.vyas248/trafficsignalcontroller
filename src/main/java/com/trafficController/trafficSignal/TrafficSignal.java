package com.trafficController.trafficSignal;

/**
 * 
 * @author Kruti Vyas
 *
 */
public class TrafficSignal{
	
	private String name;
	private Color left;
	private Color right;
	private Color front;
	private Color walk;
	
	/**
	 * gets name
	 * @return name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * sets the name
	 * @param name the String
	 */

	public void setName(String name) {
		this.name = name;
	}

	/**
	 *  gets the color of left signal
	 * @return Color the enum
	 */
	public Color isLeft() {
		return left;
	}

	/**
	 * sets the color of left signal
	 * @param left the Color
	 */
	public void setLeft(Color left) {
		this.left = left;
	}

	/**
	 *  gets the color of right signal
	 * @return Color the enum
	 */
	public Color isRight() {
		return right;
	}

	/**
	 * sets the color of right signal
	 * @param right the Color
	 */
	public void setRight(Color right) {
		this.right = right;
	}

	/**
	 *  gets the color of front signal
	 * @return Color the enum
	 */
	public Color isFront() {
		return front;
	}

	/**
	 * sets the color of front signal
	 * @param front the Color
	 */
	public void setFront(Color front) {
		this.front = front;
	}

	/**
	 *  gets the color of walk signal
	 * @return Color the enum
	 */
	public Color isPedestrian() {
		return walk;
	}

	/**
	 * sets the color of walk signal
	 * @param pedestrian the Color
	 */
	public void setPedestrian(Color pedestrian) {
		this.walk = pedestrian;
	}

	/**
	 * Constructor of Traffic signal with the name
	 * @param name the name of traffic signal
	 */
	public TrafficSignal(String name)
	{
		this.name = name;
		this.left=Color.RED;
		this.right=Color.RED;
		this.front=Color.RED;
		this.walk=Color.RED;
	}
	/**
	 * truns the left right and front signls on	 *
	 */
	public void setSignalOn()
	{
		this.left=Color.GREEN;
		this.right=Color.GREEN;
		this.front=Color.GREEN;
		this.walk=Color.RED;
	}
	/**
	 * turns the left signal on
	 */
	public void setSignalleft()
	{
		this.left=Color.GREEN;
		this.right=Color.RED;
		this.front=Color.RED;
		this.walk=Color.RED;
	}
	/**
	 * turns the pedestrian signal on 
	 */
	public void setpedestrian()
	{
		this.left=Color.RED;
		this.right=Color.RED;
		this.front=Color.RED;
		this.walk=Color.GREEN;
	}
	/**
	 * turns orange signal on
	 */
	public void setSignalOrange()
	{
		this.left=Color.GREEN;
		this.right=Color.ORANGE;
		this.front=Color.ORANGE;
		this.walk=Color.RED;
	}
	
	/**
	 * converts the singal object into string to print
	 */
	public String toString()
	{
		StringBuilder str=new StringBuilder();
		str.append("\nSide : ");
		str.append(name);
		str.append("\nLeft : ");
		str.append(left.toString());
		str.append("\nRight : ");
		str.append(right.toString());
		str.append("\nFront : ");
		str.append(front.toString());
		str.append("\nWalk : ");
		str.append(walk.toString());
		return str.toString();
	}
}
