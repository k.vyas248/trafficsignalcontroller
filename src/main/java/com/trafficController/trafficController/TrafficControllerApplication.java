package com.trafficController.trafficController;
import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * TrafficControllerApplication is the main class to start spring boot application.
 *	
 * @author Kruti Vyas
 * 
 * TrafficControllerApplication 
 *
 */
@SpringBootApplication
public class TrafficControllerApplication {

	
	@Autowired
	TrafficController trafficController;
	
	/**
	 * Main mehtod starts the run method for spring boot application.
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(TrafficControllerApplication.class, args);
	}
	
	/**
	 * init method is used for running traffic signal application.
	 * 
	 */
	@PostConstruct
	public void init()
	{
		trafficController.runTrafficSignal();		
	}
}
