package com.trafficController.trafficController;

import java.nio.channels.InterruptedByTimeoutException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.trafficController.trafficSignal.TrafficSignal;
/**
 * 
 * @author Kruti Vyas
 *
 */
@Component
public class TrafficController {
	
	/**
	 * creates traffic signals for all the sides and sets the signal colors. 
	 */
	public void runTrafficSignal()
	{
		List<TrafficSignal> signals= init();
		setSignals(signals);
	}
	
	
	/**
	 * init methods creates the list of all 4 traffic signals
	 * @return List<TrafficSignal> the list of traffic signal objects 
	 */
	public List<TrafficSignal> init()
	{
		List<TrafficSignal> signals= new ArrayList<>();
		signals.add(new TrafficSignal("NORTH"));
		signals.add(new TrafficSignal("WEST"));
		signals.add(new TrafficSignal("SOUTH"));
		signals.add(new TrafficSignal("EAST"));
		
		return signals;
		
	}
	
	/**
	 * changes the signals according to the timings
	 * @param signals the List<TrafficSignal>
	 */
	public void setSignals(List<TrafficSignal> signals)
	{
		int i=0,j;
		for(i=0,j=0;;i=(i+1)%6)
		{
			switch (i) {
			case 0:
			case 1:
			case 3:
			case 4:
				setSignalOn(signals,j);
				j+=(j+1)%4;				
				break;
			case 2:
			case 5:
				setpedestrian(signals);
				break;
			}			
		}
	}
	
	/**
	 * prints the value of traffic signal
	 * @param signals the list of traffic signals
	 */
	public void printSignals(List<TrafficSignal> signals)
	{
		System.out.println("============================================================");
		for(TrafficSignal signal:signals)
		{
			System.out.println(signal.toString());
		}
		System.out.println("============================================================");
	}
	
	/**
	 * turns on the ith signal
	 * @param signals the list of traffic signals
	 * @param i the index of signal to be turned on
	 */
	public void setSignalOn(List<TrafficSignal> signals,int i)
	{
		int k=0;
		try {
			for(TrafficSignal signal:signals)
			{
				if(k==i)
					signal.setSignalOn();
				else
					signal.setSignalleft();
				k++;
			}
			k=0;
			printSignals(signals);
			sleep(30,11);
			signals.get(i).setSignalOrange();
			printSignals(signals);
			sleep(10,0);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * prints the waiting time
	 * @param startTime the int
	 * @param endTime the int
	 * @throws InterruptedException
	 */
	public void sleep(int startTime, int endTime) throws InterruptedException
	{
		for(int i=startTime;i>=endTime;i--)
		{
			System.out.print(i+" ");
			Thread.sleep(1000);
		}
		System.out.println();
	}
	
	/**
	 * turns the signal on for pedestrians
	 * @param signals the list of traffic signals
	 */
	public void setpedestrian(List<TrafficSignal> signals)
	{
		try {
			for(int i=0;i<4;i++)
			{
				signals.get(i).setpedestrian();
			}
			printSignals(signals);
			sleep(10,0);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}
	

}
